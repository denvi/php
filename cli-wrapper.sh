#!/usr/bin/sh

# grab entrypoint from command
cmd=$1
entrypoint=${cmd##*/}
shift;

# parse version
if echo $entrypoint | grep -q "-"; then
    # grab version from filename (eg.: php-7.1)
    version=$(echo $entrypoint | cut -d "-" -f 2- )
    # remove version from entrypoint
    entrypoint=$(echo $entrypoint | cut -d "-" -f 1 )
elif [ -f "$PWD/$entrypoint.denvi" ]; then
    # grab version from file override
    version=`cat $PWD/$entrypoint.denvi`
else
    # set default version
    version="$DEFAULT_VERSION"
fi

# grab image ref
if [ -z ${image+x} ]; then 
    image="$entrypoint.denvi"
fi

# check if image is existant, build otherwise
if podman image ls -f reference=$image --format='{{.Tag}}' | grep -q "$version"; then
    podman run -it -v /:/host -u root --entrypoint $entrypoint -w /host/$PWD $image:$version $@
else
    echo "Building $image:$version..."
    if podman build --build-arg PHP_VERSION=$version -f $dockerfile -t $image:$version --logfile="logs/build.$image.$version.log" .; then
        echo "Building succesfull! Run the same command again to use the new image!"
    else 
        echo "Error while building $dockerfile with $image:$version"
        exit 1
    fi
fi
